package com.mycrud.App.service;

import java.util.List;

import com.mycrud.App.entity.Employee;

public interface EmployeeInterface {

	public void saveData(Employee e);
	
	public List<Employee> getData();
	
	public Employee getEmpById(Long id);
	
	public void updateEmpById(Long id, Employee e);
	
	public void updateEmpDetail(Employee e);
	
	public void deleteEmpById(Long id);
	
	public void deletAllEmp();
}
