package com.mycrud.App.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mycrud.App.entity.Employee;

@Repository
public interface EmployeeDaoInterface extends CrudRepository<Employee, Long>{
	

}
